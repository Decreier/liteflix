import React from 'react';
import './styles/Input.scss';

const Input = props => {
	return (
		<input
			id={props.name}
			className="input"
			type={props.type || 'text'}
			name={props.name || 'input'}
			value={props.value}
			onChange={e => props.onChange && props.onChange(e)}
			style={props.style}
			placeholder={props.title}
		/>
	);
};

export default Input;
