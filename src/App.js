import React, { createContext, useState } from 'react';
import './styles/App.scss';
import Landing from './screens/Landing';

export const ThemeContext = createContext();

const App = () => {
	const [context, changeContext] = useState({});
	let auth = true;

	return auth ? (
		<ThemeContext.Provider value={[context, changeContext]}>
			<Landing />
		</ThemeContext.Provider>
	) : null;
};

export default App;
