import React, { useEffect } from 'react';
import Card from '../components/Card';
import './styles/Movies.scss';

const Movies = ({ proximamente, populares, mias }) => {
	return (
		<div className="movies">
			<p>Próximamente</p>
			<div className="cards">
				{proximamente &&
					proximamente.map(
						(mov, i) =>
							i < 4 && (
								<Card
									key={mov.id}
									poster={`https://image.tmdb.org/t/p/w500/${mov.poster_path}`}
									title={mov.title}
									small
								/>
							)
					)}
			</div>
			{mias.length > 0 && <p>Mi Lista</p>}
			<div className="cards">
				{mias.length > 0 &&
					mias.map(
						(mov, i) =>
							i < 4 && (
								<Card
									key={mov.id}
									poster={`https://image.tmdb.org/t/p/w500/${mov.poster_path}`}
									title={mov.title}
									small
								/>
							)
					)}
			</div>
			<p>POPULARES DE NETFLIX</p>
			<div className="cards">
				{populares &&
					populares.map(
						(mov, i) =>
							i < 4 && (
								<Card
									key={mov.id}
									poster={`https://image.tmdb.org/t/p/w500/${mov.poster_path}`}
									title={mov.title}
								/>
							)
					)}
			</div>
		</div>
	);
};

export default Movies;
