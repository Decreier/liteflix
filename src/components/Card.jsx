import React from 'react';
import './styles/Card.scss';
import playButton from '../assets/play-button.svg';

const Card = props => {
	return (
		<div
			className="card"
			style={{
				backgroundImage: `url(${props.poster})`,
				backgroundPosition: 'bottom',
				backgroundSize: 'cover',
				backgroundRepeat: 'no-repeat',
				width: 'stretch',
				height: props.small ? '140px' : '360px'
			}}
		>
			{props.small && <h1>LITEFLIX</h1>}
			<div className="card_info">
				<img fill="yellow" src={playButton} className="card_info_play" alt="play" />
			</div>
		</div>
	);
};

export default Card;
