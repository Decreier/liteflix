import React from 'react';
import './styles/Dialog.scss';

const Dialog = props => {
	return (
		props.show && (
			<div className="dialog">
				<div className="dialog_box">{props.children}</div>
			</div>
		)
	);
};

export default Dialog;
