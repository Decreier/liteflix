import React, { useEffect, useState } from 'react';
import Button from '../components/Button';
import './styles/Nav.scss';
import add from '../assets/plus.svg';
import Dialog from '../components/Dialog';
import { useModal } from '../hooks/useModal';
import Input from '../components/Input';

const Nav = () => {
	const [open, change] = useModal();
	const [newMovie, setNewMovie] = useState({});

	const handleMovie = (name, value) => {
		setNewMovie({ ...newMovie, [name]: value });
	};

	const addMovie = () => {
		let mias = localStorage.getItem('mias');
		localStorage.setItem('mias', [...mias, newMovie]);
		change();
		setNewMovie({});
	};

	return (
		<div className="nav">
			<div>
				<h2>liteflix</h2>
				<a>
					<b>Inicio</b>
				</a>
				<a>Series</a>
				<a>Películas</a>
				<a>Agregadas recientemente</a>
				<a>Mi lista</a>
				<Button add icon={add} onClick={() => change()}>
					Agregar Pelicula
				</Button>
			</div>
			<div>
				<p>Niños</p>
				<p>N</p>
				<p>O</p>
				<p>-</p>
			</div>
			<Dialog show={open}>
				<Input
					type="file"
					name="poster_path"
					style={{ background: 'lightGray', padding: '30px', border: 'none' }}
					onChange={e => handleMovie(e.target.name, e.target.value)}
				/>
				<div>
					<Input
						title="NOMBRE DE LA PELLICULA"
						name="title"
						onChange={e => handleMovie(e.target.name, e.target.value)}
					/>
					<Input
						onChange={e => handleMovie(e.target.name, e.target.value)}
						title="CATEGORIA"
						name="cat"
						value="mias"
					/>
				</div>
				<Button
					onClick={() => addMovie()}
					disabled={!newMovie.title || !newMovie.cat || !newMovie.poster_path}
				>
					Agregar película
				</Button>
			</Dialog>
		</div>
	);
};

export default Nav;
