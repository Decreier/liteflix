import React from 'react';
import './styles/Button.scss';

const Button = props => {
	return (
		<button
			className="button"
			style={{
				background: props.add
					? 'rgb(229,9,20)'
					: props.disabled
					? 'lightGray'
					: 'rgba(0, 0, 0, 0.555)',
				marginLeft: props.add && '20px',
				paddingLeft: !props.add && '20px',
				color: !props.add && 'white',
				width: !props.add && '170px'
			}}
			disabled={props.disabled}
			type="button"
			onClick={() => props.onClick && props.onClick()}
		>
			{props.icon && (
				<img
					src={props.icon}
					alt="icon"
					style={{ width: '18px', height: '18px', marginRight: '7px' }}
				/>
			)}
			{props.children}
		</button>
	);
};

export default Button;
