import React, { useEffect, useState } from 'react';
import './styles/Landing.scss';
import Nav from '../layout/Nav';
import Promo from '../layout/Promo';
import Movies from '../layout/Movies';
import { useMovies } from '../hooks/useMovies';

const Landing = () => {
	const [destacadas, proximamente, populares, mias] = useMovies();
	const [destacada, setDestacada] = useState();

	useEffect(() => {
		setDestacada(destacadas[2]);
	}, [destacadas]);

	return (
		<div className="landing">
			<div
				className="landing_head"
				style={{
					backgroundImage: `url(https://image.tmdb.org/t/p/original/${
						destacada && destacada.backdrop_path
					})`,
					backgroundSize: 'cover',
					backgroundRepeat: 'no-repeat'
				}}
			>
				<Nav />
				<Promo destacada={destacada} />
			</div>
			<div className="landing_body">
				<Movies proximamente={proximamente} populares={populares} mias={mias} />
			</div>
		</div>
	);
};

export default Landing;
