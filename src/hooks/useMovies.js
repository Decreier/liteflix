import { useEffect, useState } from 'react';
import axios from 'axios';

export const useMovies = () => {
	const [destacadas, setDestacadas] = useState([]);
	const [proximamente, setProximamente] = useState([]);
	const [populares, setPopulares] = useState([]);
	const [mias, setMias] = useState([]);
	const [uris] = useState({
		destacadas:
			'https://api.themoviedb.org/3/movie/now_playing?api_key=6f26fd536dd6192ec8a57e94141f8b20',
		proximamente:
			'https://api.themoviedb.org/3/movie/upcoming?api_key=6f26fd536dd6192ec8a57e94141f8b20',
		populares: 'https://api.themoviedb.org/3/movie/popular?api_key=6f26fd536dd6192ec8a57e94141f8b20'
	});

	let getDestacadas = async () => {
		let dest = localStorage.getItem('destacadas') || [];
		await axios.get(uris.destacadas).then(res => setDestacadas([...res.data.results, ...dest]));
	};

	let getProximamente = async () => {
		let prox = localStorage.getItem('proximamente') || [];
		await axios.get(uris.proximamente).then(res => setProximamente([...res.data.results, ...prox]));
	};

	let getPopulares = async () => {
		let popu = localStorage.getItem('populares') || [];
		await axios.get(uris.populares).then(res => setPopulares([...res.data.results, ...popu]));
	};

	let getMias = () => {
		let mias = localStorage.getItem('mias') || [];
		setMias([...mias]);
	};

	useEffect(() => {
		getDestacadas();
		getProximamente();
		getPopulares();
		getMias();
	}, []);

	return [destacadas, proximamente, populares, mias];
};
