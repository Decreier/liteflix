import React from 'react';
import Button from '../components/Button';
import './styles/Promo.scss';
import play from '../assets/play.svg';
import plus from '../assets/plus.svg';

const Promo = ({ destacada }) => {
	return (
		<div className="promo">
			<span>
				ORIGINAL DE <b>LITEFLIX</b>
			</span>
			<h1>{destacada && destacada.title}</h1>
			<div className="promo_buttons">
				<Button icon={play}>Reproducir</Button>
				<Button icon={plus}>Mi Lista</Button>
			</div>
			<h3>Ver Película</h3>
			<p>{destacada && destacada.overview}</p>
		</div>
	);
};

export default Promo;
