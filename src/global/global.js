export const globalStates = {
	user: '',
	methods: []
};

export const globalActions = {
	add: (methods, newMethod) => methods.push(newMethod),
	update: (methods, index, updatedMethods) => methods.splice(index, 1, updatedMethods),
	delete: (methods, index) => methods.splice(index, 1)
};
